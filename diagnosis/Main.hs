{-# LANGUAGE OverloadedStrings #-}
import Control.Lens
import Data.Default

import Language
import Compiler
import Runtime hiding (Cst, Fun)
import qualified Runtime  as R
import qualified Data.Map as Map


module1 :: Module ()
module1 = buildModule $ do

    "double"      ~> \x   -> (Var x) .+. (Var x)
    "doubleApply" ~> \f x -> (Var f) @@ ((Var f) @@ (Var x))
    "main"        ~> ((Fun "doubleApply") @@ (Fun "double") @@ (Cst 5))


-- main = wholeCompiler
main = gmachineTest

wholeCompiler :: IO ()
wholeCompiler = do 
    let mainModule = module1
    let Right compiledCode = compileModule mainModule
    let config = def & set silent True
    (_, state) <- withCode compiledCode $ debugger config


    printStats state

gmachineTest :: IO ()
gmachineTest = do 
    let facDef =
          [ Push (Arg 1)        -- [n]
          , Eval           
          , Skip NZ 2           -- []
          , Push (R.Cst 1)      -- [1] | conditional branch 1
          , Skip NoCond 11      --     |
          , Push (Arg 1)        -- [n]               | 
          , Eval                --                   | 
          , Push (R.Cst 1)      -- [n, 1]            | 
          , Eval                --                   | 
          , Push (Arg 3)        -- [n, 1, n]         |  conditional branch 2
          , Eval                --                   | 
          , PrimOp R.Sub        -- [n, n - 1]        | 
          , Push (Global "fac") -- [n, n - 1, fac]   | 
          , MkAp                -- [n, fac (n - 1)]  | 
          , Eval                --                   |
          , PrimOp R.Mul        -- [n * fac (n - 1)] | 
          , Overwrite 2    
          , Reduce       ]
    

    let mainDef =
         [ Push (R.Cst 3)
         , Push (Global  "fac")
         , MkAp
         , Overwrite 1
         , Reduce                ]    

    let code = Map.fromList $
                [ ("main",   mainDef )
                , ("fac",    facDef  ) ]

    (_, state) <- withCode code $ debugger def
    printStats state


module Compiler where

import Data.List (findIndex)
import Data.Map  (Map)
import qualified Data.Map as Map
import Data.Monoid hiding (Ap)

import Language
import qualified Runtime as R


compileModule :: Module ann -> Either CompileError (Map Fun R.GMCode)
compileModule mainModule = 
    maybe
        (Right $ fmap compile mainModule) 
        Left
        (checkCorrectness mainModule)

compile :: FunDef ann -> R.GMCode
compile (FunDef args body) = reverse $ R.Reduce:(R.Overwrite n):(compileAux args body [])
                             where n = length args + 1


compileAux :: [Var] -> Expr ann -> R.GMCode -> R.GMCode
compileAux args body code = case body of 
          Cst val -> (R.Push (R.Cst    $ R.Value val)):code
          Fun fun -> (R.Push (R.Global fun)):code
          Var var -> (R.Push (R.Arg    offset)):code where
                     offsetToBeg = sum $ map stackIncrement $ code
                     offsetToArg = maybe
                                     (error "Can't find var !")
                                     (+1) 
                                     (findIndex (== var) args)
                     offset = offsetToBeg + offsetToArg
                     stackIncrement (R.Push _)   = 1
                     stackIncrement R.MkAp       = -1
                     stackIncrement (R.PrimOp _) = -1
                     stackIncrement _            = 0


          Add expr1 expr2 -> let codeExpr1 = compileAux args expr1 code
                                 codeExpr2 = compileAux args expr2 $ R.Eval:codeExpr1
                             in (R.PrimOp R.Add):R.Eval:codeExpr2

          Sub expr1 expr2 -> let codeExpr1 = compileAux args expr1 code
                                 codeExpr2 = compileAux args expr2 $ R.Eval:codeExpr1
                             in (R.PrimOp R.Sub):R.Eval:codeExpr2

          Mul expr1 expr2 -> let codeExpr1 = compileAux args expr1 code
                                 codeExpr2 = compileAux args expr2 $ R.Eval:codeExpr1
                             in (R.PrimOp R.Mul):R.Eval:codeExpr2

          IfNZ expr1 expr2 expr3 -> let codeExpr1 = compileAux args expr1 code
                                        codeExpr2 = compileAux args expr2 $ R.Eval:codeExpr1
                                        codeExpr3 = compileAux args expr3 $ R.Eval:codeExpr2
                                    in (R.PrimOp R.IfNZ):R.Eval:codeExpr2

          Ap expr1 expr2  -> let codeExpr2 = compileAux args expr2 code
                                 codeExpr1 = compileAux args expr1 codeExpr2
                             in R.MkAp:codeExpr1
          Annotate _ expr -> compileAux args expr code

type ErrorLoc = Fun

data CompileError
    = UnknownArg Var
    | UnknownGlobal Fun
    | TypeError
    | ReuseVar Var
    | Loc ErrorLoc CompileError
    deriving (Eq, Show)

checkCorrectness :: Module ann -> Maybe CompileError
checkCorrectness mainMod = 
    getFirst $ 
    mconcat  $ 
    [First $ check mainMod | check <- [checkArg, checkGlobal]]

checkArg :: Module ann -> Maybe CompileError
checkArg mainMod = getFirst $ foldMap checkArgFunc (Map.elems mainMod) where
    checkArgFunc (FunDef args body) = checkExpr args body
    checkExpr args (Var var) 
        | elem var args = mempty
        | otherwise     = return $ UnknownArg var
    checkExpr args (Ap  f x)      = foldMap (checkExpr args) [f, x]
    checkExpr args (Add x y)      = foldMap (checkExpr args) [x, y]
    checkExpr args (Sub x y)      = foldMap (checkExpr args) [x, y]
    checkExpr args (Mul x y)      = foldMap (checkExpr args) [x, y]
    checkExpr args (IfNZ c x y)   = foldMap (checkExpr args) [c, x, y]
    checkExpr args (Annotate _ e) = checkExpr args e
    checkExpr _ _ = mempty





checkGlobal :: Module ann -> Maybe CompileError
checkGlobal mainMod = getFirst $ foldMap checkGlobalFunc (Map.elems mainMod) where
    checkGlobalFunc (FunDef _ body) = checkExpr body
    checkExpr (Fun name) 
        | Map.member name mainMod = mempty
        | otherwise               = return $ UnknownGlobal name
    checkExpr (Ap  f x)      = foldMap checkExpr [f, x]
    checkExpr (Add x y)      = foldMap checkExpr [x, y]
    checkExpr (Sub x y)      = foldMap checkExpr [x, y]
    checkExpr (Mul x y)      = foldMap checkExpr [x, y]
    checkExpr (IfNZ c x y)   = foldMap checkExpr [c, x, y]
    checkExpr (Annotate _ e) = checkExpr e
    checkExpr _ = mempty

checkUniqueUse :: Module ann -> Maybe CompileError
checkUniqueUse mainMod = getFirst $ foldMap checkUniqueUseFunc (Map.elems mainMod) where
    checkUniqueUseFunc (FunDef args body) =
        case filter ((> 1) . (\arg -> countUses arg body)) args of 
            var:_  -> First $ Just $ ReuseVar var
            _      -> First $ Nothing
    countUses :: Var -> Expr ann -> Int
    countUses arg (Var name) 
        | arg == name  = 1
        | otherwise    = 0
    countUses arg (Ap  f x)    = sum $ countUses arg <$> [f, x]
    countUses arg (Add x y)    = sum $ countUses arg <$> [x, y]
    countUses arg (Sub x y)    = sum $ countUses arg <$> [x, y]
    countUses arg (Mul x y)    = sum $ countUses arg <$> [x, y]
    countUses arg (IfNZ c x y) = sum $ countUses arg <$> [c, x, y]
    countUses arg (Annotate _ e) = countUses arg e
    countUses _ _ = 0



{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE OverloadedStrings #-}
module Runtime where

import Control.Lens
import Control.Monad.State
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe
import Data.Default

import qualified Language as L (Fun)

newtype Addr   = Addr   Int deriving (Num, Eq, Show, Ord)
newtype Value  = Value  Int deriving (Num, Eq, Show, Ord)

data GMOp
    = Add
    | Sub
    | Mul
    | IfNZ
    deriving (Eq, Show)

arityOp :: GMOp -> Int
arityOp Add  = 2
arityOp Sub  = 2
arityOp Mul  = 2
arityOp IfNZ = 3

toHaskOp :: GMOp -> [Value] -> Value
toHaskOp Add  (x:y:_)   = x + y
toHaskOp Sub  (x:y:_)   = x - y
toHaskOp Mul  (x:y:_)   = x * y
toHaskOp IfNZ (x:y:z:_) 
    | x == 0    = y 
    | otherwise = z
toHaskOp _ _ = error "Not enough operands"

data GMNode  
    = App Addr Addr
    | Val Value
    | Fun L.Fun
    deriving (Eq, Show)


type Heap   = Map Addr GMNode
type Stack  = [Addr]
type GMCode = [GMInstr]
type Dump   = [(Stack, GMCode)]

data GMInstr 
    = Push PushVal
    | MkAp
    | Reduce
    | PrimOp GMOp
    | Eval   -- = reduce + call
    | Skip Condition Int
    | Return
    | Overwrite Int
    deriving (Eq, Show)

data Condition
    = NZ
    | NoCond
    deriving (Eq, Show)

data PushVal 
    = Global L.Fun
    | Cst    Value
    | Arg    Int
    deriving (Eq, Show)


data RuntimeStats = RuntimeStats {
    _nAllocations :: Int,
    _maxHeapSize  :: Int,
    _heapSize     :: Int,

    _nSteps       :: Int,
    _nCalls       :: Int,
    _nCopies      :: Int
} deriving (Eq, Show)
makeLenses ''RuntimeStats
instance Default RuntimeStats where
    def = RuntimeStats {
        _nAllocations = 0,
        _maxHeapSize  = 0,
        _heapSize     = 0,

        _nSteps       = 0,
        _nCalls       = 0,
        _nCopies      = 0
    }

data GMState = GMState {
      _dump      :: Dump
    , _current   :: Stack
    , _heap      :: Heap
    , _nextAddr  :: Addr

    , _nextInstr   :: GMCode
    , _labels      :: Map L.Fun GMCode

    , _stats     :: RuntimeStats
} deriving (Eq, Show)
makeLenses ''GMState

newtype GM m a = GM {_unwrapGM :: StateT GMState m a}
               deriving (Functor, Applicative, Monad, MonadState GMState)
deriving instance MonadIO m => MonadIO (GM m)

step :: (Monad m) => GM m Bool
step = do
    next <- uses nextInstr uncons
    case next of
        Nothing  -> return False
        Just (instr, rest) -> do
            modifying (stats . nSteps) (+1)
            nextInstr .= rest
            runInstr instr
            return True

runTillEnd :: (Monad m) => GM m GMNode
runTillEnd = do
    let loop = do
            notDone <- step
            when notDone loop
    loop
    addr <- pop
    uses heap $ flip (Map.!) addr


debugger :: DebuggerConfig -> GM IO ()
debugger DebuggerConfig{..} = do
    let loop (Just 0) = when (not _silent) $ liftIO $ putStrLn "Interrupted !"
        loop n = do
            when (not _silent) $ do
                printStack
                printFuture
                liftIO $ putStrLn "\n\n"

            notDone <- step
            when notDone $ loop $! fmap (subtract 1) n
    loop _stopAt
    liftIO $ putStrLn "DONE !"
    -- addr <-


runInstr :: (Monad m) => GMInstr -> GM m ()
runInstr (Push pushVal) = do 
    let getArg offset = do
            val  <- uses current $ flip (!!) offset  
            node <- retrieve val
            case node of
                App _ addr -> return addr 
                _          -> error "Not an application!"
    toPush <- case pushVal of 
                Global string -> allocate (Fun string)
                Cst    value  -> allocate (Val value)
                Arg    offset -> getArg offset
    push toPush

-- Skip n-many future instruction based on value at top of stack (requires value to be evaluated)
runInstr (Skip cond leapSize) = do 

    bool <- case cond of 
              NoCond -> return True
              _      -> do
                  addr <- pop
                  val  <- retrieve addr
                  case val of
                    Val num -> return $ case cond of
                                  NZ -> num /= 0
                                  _  -> True
                    _ -> error "Input to conditional must be evaluated"

    when bool $ 
        modifying nextInstr $ drop leapSize

-- copy the return value to address
runInstr (Overwrite offset) = do 
    oldVal <- uses current $ flip (!!) offset  
    newVal <- uses current $ head
    move newVal oldVal

    -- forM_ [1..offset] $ const $ do
    --     addr <- pop
    --     free addr
    modifying current $ drop offset


runInstr MkAp = do
    nodes <- popN 2
    case nodes of 
        node1:node2:_ -> do
            addr <- allocate (App node1 node2)
            push addr
            -- return ()
        _ -> error "Not enough nodes for application"
    -- node1:node2:_

    return ()
 
runInstr Reduce = do 
    addr <- uses current head
    node <- retrieve addr
    reduce node

runInstr Eval   = callEval
runInstr Return = ret
runInstr (PrimOp op) = do
    operandAddrs   <- popN $ arityOp op
    boxedOperands  <- mapM retrieve operandAddrs
    let operands = flip map boxedOperands $ \case
                        Val x -> x
                        _     -> error "Unevaluated operand"

    addr <- allocate (Val $ toHaskOp op operands)
    push addr


reduce :: (Monad m) => GMNode -> GM m ()
reduce (App f _) = do
    push f
    modifying nextInstr (Reduce:)

reduce (Fun label) = call label

reduce (Val _) = return ()

------------------- UTILS -----------------
push :: (Monad m) => Addr -> GM m ()
push addr = current %= (addr:) 

popN :: (Monad m) => Int -> GM m [Addr]
popN n = do 
    (nodes, restStack) <- uses current (splitAt n)
    current .= restStack
    return nodes


pop :: (Monad m) => GM m Addr
pop = do 
    maybeAddr <- uses current uncons 
    maybe 
        (error "Nothing to pop!") 
        (\(addr, rest) -> do
            current .= rest
            return addr)
        maybeAddr

call :: (Monad m) => L.Fun -> GM m ()
call label = do
    code      <- uses labels $ flip (Map.!) label
    nextInstr %= \instrs -> code ++ instrs
    modifying (stats . nCalls) (+1)


callEval :: (Monad m) =>  GM m ()
callEval = do
    toEval    <- pop
    oldStack  <- use current
    oldCode   <- use nextInstr

    modifying dump ((oldStack, oldCode):)

    current   .= [toEval]
    nextInstr .= [Reduce, Return]

ret :: (Monad m) => GM m () 
ret = do
    prev <- use dump
    case prev of 
        (stack, instrs):restCallStack -> do
            addr <- pop
            nextInstr .= instrs
            current   .= stack
            push addr
            dump .= restCallStack

        _ -> return ()

allocate :: (Monad m) => GMNode -> GM m Addr
allocate node = do
    addr <- use nextAddr
    heap %= Map.insert addr node
    nextAddr .= addr + 1

    -- Stats
    modifying (stats . nAllocations) (+1)
    modifying (stats . heapSize    ) (+1)
    currentSize <- use $ stats . heapSize
    modifying (stats . maxHeapSize ) (max currentSize)
    return addr


free :: (Monad m) => Addr -> GM m ()
free addr = do
    heap %= Map.delete addr
    modifying (stats . heapSize) (subtract 1)



retrieve :: (Monad m) => Addr -> GM m GMNode
retrieve addr = do
    maybeNode <- uses heap (Map.lookup addr)
    let err = error "Address does not exist."
    return $ fromMaybe err maybeNode

move :: (Monad m) => Addr -> Addr -> GM m ()
move fromAddr toAddr = do
    newVal <- uses heap $ flip (Map.!) fromAddr
    heap %= Map.insert toAddr newVal
    heap %= Map.delete fromAddr
    modifying (stats . nCopies) (+1)

------------------- RUN -----------------

withCode :: (Monad m) => Map L.Fun GMCode -> GM m a -> m (a, GMState)
withCode code run = 
    run                           &
    _unwrapGM                     &
    flip runStateT initialState 
    where _dump      = []
          _current   = []
          _heap      = Map.empty
          _nextAddr  = 0
          _nextInstr = [Push (Global "main"), Reduce]
          _labels    = code
          _stats     = def
          initialState = GMState{..}

------------------- IO -----------------

printStack :: GM IO ()
printStack = do
    liftIO $ putStrLn "=== Stack ==="
    stack_ <- use current
    forM_ stack_ $ \addr -> do
        node <- retrieve addr
        liftIO $ putStr $ show addr
        liftIO $ putStr " : "
        liftIO $ print  node
    liftIO $ putStrLn "============="

printFuture :: GM IO ()
printFuture = do
    liftIO $ putStrLn "=== Next Instr ==="
    future <- uses nextInstr $ take 10
    forM_ future $ \instr ->
        liftIO $ print instr
    liftIO $ putStrLn "============="


printStats :: GMState -> IO ()
printStats state = do
    putStr "nAllocations : " 
    print $ state^.stats.nAllocations
    putStr "maxHeapSize  : "  
    print $ state^.stats.maxHeapSize
    putStr "heapSize     : "     
    print $ state^.stats.heapSize
    putStr "nSteps       : "       
    print $ state^.stats.nSteps
    putStr "nCalls       : "       
    print $ state^.stats.nCalls
    putStr "nCopies      : "       
    print $ state^.stats.nCopies

--
data DebuggerConfig = DebuggerConfig {
    _silent :: Bool,
    _stopAt :: Maybe Int
} deriving (Eq, Show)
makeLenses ''DebuggerConfig
instance Default DebuggerConfig where
    def = DebuggerConfig {
        _silent = False,
        _stopAt = Nothing
    }

module TypeInference where

import Language

import Data.Maybe
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Void
import Control.Lens
import Control.Monad.State
import Control.Monad.Except


------------------- TYPES -----------------

data Name 
    = NamedVar    Fun Var 
    | NamedGlobal Fun
    | Anonymous Int 
    deriving (Eq, Show, Ord)



data HoledType a
    = Val
    | (:->) (HoledType a) (HoledType a)
    | FreeVar a
    deriving (Show, Eq, Ord, Functor, Foldable, Traversable)

type Type      = HoledType Void
type Contraint = HoledType ()

data TypeEquation a = (:~:) (HoledType a) (HoledType a) deriving (Eq, Show, Ord)
type TypeSubst a    = Map a (HoledType a)

newtype TypeMonad a b = TypeMonad {
    _unwrapTypeMonad :: ExceptT () (State (TypeSubst a)) b 
} deriving 
   ( Functor
   , Applicative
   , Monad
   , MonadError ()
   , MonadState (TypeSubst a) )

runTypeMonad :: TypeSubst a -> TypeMonad a b -> Maybe (TypeSubst a, b)
runTypeMonad state monad = 
    mkMaybe                   $
    runIdentity               $
    flip runStateT state      $
    runExceptT                $
    _unwrapTypeMonad monad
    where mkMaybe (Left (), _)  = Nothing
          mkMaybe (Right x, s)  = Just (s, x)


unify :: (Ord a) => TypeEquation a ->  TypeMonad a (HoledType a)
unify ((FreeVar v) :~: x) = do 
    appendSubst v x
    gets $ flip (Map.!) v
unify ((a :-> b) :~: (c :-> d)) = do
    arg <- unify $ a :~: c
    out <- unify $ b :~: d
    return $ arg :-> out
unify ((_ :-> _) :~: Val) = throwError ()
unify (Val :~: Val)       = return Val
unify (x :~: y) = unify $ y :~: x

appendSubst :: (Ord a) => a -> HoledType a -> TypeMonad a () 
appendSubst var body = do
    maybeBody <- gets (Map.lookup var)
    case maybeBody of 
        Nothing     -> do 
            modify' $ Map.insert var body
            return body
        Just body'  -> do
            unification <- unify $ body :~: body'
            modify' $ Map.insert var unification
            return unification
    return ()

applySubstAux :: (Ord a) => HoledType a -> TypeMonad a (HoledType a)
applySubstAux Val = return Val
applySubstAux fv@(FreeVar v) = do
    maybeBody <- gets $ Map.lookup v
    return $ fromMaybe fv maybeBody

applySubstAux (a :-> b) = do
    arg <- applySubstAux a
    out <- applySubstAux b
    return $ arg :-> out

applySubst :: (Ord a) => TypeMonad a ()
applySubst = get >>= mapM applySubstAux >>= put
    -- oldState <- get
    -- newState <- mapM applySubstAux get 
    -- put newState

infiniteTypeCheck :: (Eq a) => TypeMonad a ()
infiniteTypeCheck = do
    typeValues <- gets Map.toList
    forM_ typeValues $ \(v, ty) -> 
        when (any (== v) ty) $ 
            throwError ()



labelAllSubExpr :: Fun -> Expr ann -> State Int (Expr Name)
labelAllSubExpr _ (Cst c) = do 
    i <- newIndex
    return $ Annotate (Anonymous i) (Cst c)

labelAllSubExpr fName (Add e1 e2)  = do
        e1Ann <- labelAllSubExpr fName e1
        e2Ann <- labelAllSubExpr fName e2
        i <- newIndex
        return $ Annotate (Anonymous i) $ Add e1Ann e2Ann

labelAllSubExpr fName (Sub e1 e2)  = do
        e1Ann <- labelAllSubExpr fName e1
        e2Ann <- labelAllSubExpr fName e2
        i <- newIndex
        return $ Annotate (Anonymous i) $ Sub e1Ann e2Ann

labelAllSubExpr fName (Mul e1 e2)  = do
        e1Ann <- labelAllSubExpr fName e1
        e2Ann <- labelAllSubExpr fName e2
        i <- newIndex
        return $ Annotate (Anonymous i) $ Mul e1Ann e2Ann

labelAllSubExpr fName (IfNZ e1 e2 e3) = do
        e1Ann <- labelAllSubExpr fName e1
        e2Ann <- labelAllSubExpr fName e2
        e3Ann <- labelAllSubExpr fName e3
        i <- newIndex
        return $ Annotate (Anonymous i) $ IfNZ e1Ann e2Ann e3Ann

labelAllSubExpr fName (Ap e1 e2) = do
        e1Ann <- labelAllSubExpr fName e1
        e2Ann <- labelAllSubExpr fName e2
        i <- newIndex
        return $ Annotate (Anonymous i) $ Ap e1Ann e2Ann

labelAllSubExpr _ (Fun glob) = return $ Annotate (NamedGlobal glob) $ Fun glob
labelAllSubExpr fName (Var var)  = return $ Annotate (NamedVar fName var)  $ Var var

labelAllSubExpr fName (Annotate _ e) = 
    labelAllSubExpr fName e



newIndex :: State Int Int
newIndex = do
    i <- get
    modify (+1)
    return i

getAnnotation :: Expr ann -> ann 
getAnnotation (Annotate n _) = n
getAnnotation _ = error "Expr has no annotation"

gatherEquations :: Expr Name -> [TypeEquation Name]
gatherEquations (Annotate n1 (Add e1 e2)) = 
    [ FreeVar n1  :~: Val
    , FreeVar n1' :~: Val
    , FreeVar n2' :~: Val ] ++  
    gatherEquations e1 ++
    gatherEquations e2
    where n1' = getAnnotation e1
          n2' = getAnnotation e2

gatherEquations (Annotate n1 (Sub e1 e2)) = 
    [ FreeVar n1  :~: Val
    , FreeVar n1' :~: Val
    , FreeVar n2' :~: Val ] ++  
    gatherEquations e1 ++
    gatherEquations e2
    where n1' = getAnnotation e1
          n2' = getAnnotation e2

gatherEquations (Annotate n1 (Mul e1 e2)) = 
    [ FreeVar n1  :~: Val
    , FreeVar n1' :~: Val
    , FreeVar n2' :~: Val ] ++  
    gatherEquations e1 ++
    gatherEquations e2
    where n1' = getAnnotation e1
          n2' = getAnnotation e2


gatherEquations (Annotate n1 (IfNZ e1 e2 e3)) = 
    [ FreeVar n1  :~: Val
    , FreeVar n1' :~: Val
    , FreeVar n2' :~: Val   
    , FreeVar n3' :~: Val ] ++  
    gatherEquations e1 ++
    gatherEquations e2 ++
    gatherEquations e3
    where n1' = getAnnotation e1
          n2' = getAnnotation e2
          n3' = getAnnotation e3


gatherEquations (Annotate n1 (Ap e1 e2)) = 
    [ FreeVar n1' :~: (FreeVar n2' :-> FreeVar n1) ] ++
    gatherEquations e1 ++
    gatherEquations e2 
    where n1' = getAnnotation e1
          n2' = getAnnotation e2



gatherEquations (Annotate n1 (Cst _)) = 
    [ FreeVar n1 :~: Val ] 

gatherEquations (Annotate _ _) = [] 


gatherEquations _ = error "Expression does not have a name."

-- gatherEquationsAux :: Expr -> State Int [TypeEquation Name] 
-- gatherEquationsAux func (Annotate n1 (Add (Annotate n1' e1) (Annotate n2' e2))) = 
--     do


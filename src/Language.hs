{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE OverloadedStrings   #-}
module Language where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.String
import Control.Lens
import Control.Monad.State


data Expr ann where

    Cst ::  Int -> Expr ann
    Add ::  Expr ann -> Expr ann -> Expr ann
    Sub ::  Expr ann -> Expr ann -> Expr ann
    Mul ::  Expr ann -> Expr ann -> Expr ann
    IfNZ :: Expr ann -> Expr ann -> Expr ann -> Expr ann

    Fun  :: Fun -> Expr ann
    Var  :: Var -> Expr ann
    Ap   :: Expr ann -> Expr ann -> Expr ann

    Annotate :: ann -> Expr ann -> Expr ann

deriving instance Functor     Expr
deriving instance Foldable    Expr
deriving instance Traversable Expr
deriving instance (Show ann) => Show (Expr ann)
deriving instance (Eq   ann) => Eq   (Expr ann)

newtype Var = Var_ String deriving (IsString, Show, Eq, Ord)
newtype Fun = Fun_ String deriving (IsString, Show, Eq, Ord)

(.+.), (.-.), (.*.) :: Expr ann -> Expr ann -> Expr ann
(.+.) = Add 
(.-.) = Sub 
(.*.) = Mul 

(@@) :: Expr ann -> Expr ann -> Expr ann
(@@) = Ap


data FunDef ann = FunDef {
      _args :: [Var]
    , _body :: Expr ann
} deriving (Eq, Show)

type Module ann = Map Fun (FunDef ann)


------------------- SYNTACTIC SUGAR -----------------

data ModuleBuilderState ann = ModuleBuilderState {
    _builtModule :: Module ann
} deriving (Eq, Show)
makeLenses ''ModuleBuilderState


newtype ModuleBuilder ann a = 
    ModuleBuilder { _unwrapModuleBuilder :: State (ModuleBuilderState ann) a }
    deriving (Functor, Applicative, Monad, MonadState (ModuleBuilderState ann))

buildModule :: ModuleBuilder ann a -> Module ann
buildModule modBuilder = 
    _builtModule                                $                               
    execState (_unwrapModuleBuilder modBuilder) $
    ModuleBuilderState Map.empty 

varNames :: [Var]
varNames = fmap Var_ [var ++ (replicate i '\'') | i <- [0..], var <- ["x", "y", "z"]]

(~>) :: (ToFunDef a) => Fun -> a -> ModuleBuilder (AnnotationOf a) ()
(~>) name code = 
    builtModule %= Map.insert name body
    where body = toFunDef varNames code 



class ToFunDef a where
    type AnnotationOf a
    toFunDef :: [Var] -> a -> FunDef (AnnotationOf a)

instance ToFunDef (Expr ann) where
    type AnnotationOf (Expr ann) = ann
    toFunDef _ code = FunDef [] code 

instance (ToFunDef a) => ToFunDef (Var -> a) where
    type AnnotationOf (Var -> a) = AnnotationOf a
    toFunDef (x:l) abstract = let
        FunDef vars code = toFunDef l $ abstract x
        in FunDef (x:vars) code
    toFunDef _ _ = error "Not enough variables were provided !"



------------------- TEST COMPILATION -----------------


testModule :: Module ()
testModule = buildModule $ do

    "double" ~> (\(x :: Var) -> (Var x) .+. (Var x))
    "main"   ~> ((Fun "double") @@ Cst 4)

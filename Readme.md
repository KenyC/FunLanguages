Functional programming languages without garbage collection
===========================================================


```haskell
let g = f 3 in 
g 5 + g 4
```

```
   /\ 
  /  \
 /\   \
+  <   >
  / \ / \
 5   >   4
    / \
   f   3
```



**Question:** can we impose restriction on a functional programming language so that the compiler can know at compile-time when memory allocated to thunks should be free'd ? 


## Answer 1: use at most once

AKA linear types

With this restriction, a thunk is no longer a graph, but a simple tree. Each node has at most one mother.

If all functions use their arguments at most once, then passing arguments can be seen as transferring ownership of a thunk to a function

```haskell
-- transferring ownership of x and y to +
perimeter x y = (x + y) * 2
-- + is in charge of deallocating

-- y is dropped ; y must be free'd
width x y = x 
```


A reduction reduces a left-branching structure into something else. Each reduction can free the nodes it replaces.

  ```
                   @  <= can be free'd
                  /\
                 /  \
can be free'd =>@    \
               / \    \  
              /   \    \
             /     \    \
       perimeter  [.1.] [.2.]

==>       /\
         /  \
        / \  2
       /   \
      *    /\
          /  \
         /    \
        / \    \  
       /   \    \
      /     \    \
     +     [.1.] [.2.]

```

In case some nodes are dropped in the process of the computation (i.e. short-circuit evaluation), they can be freed then.





## Answer 2 : use at most once + copy construct

Linear types are restrictive ; what about 
import Test.Tasty

import qualified Test.Language
import qualified Test.Runtime
import qualified Test.TypeInference
import qualified Test.Compiler

main :: IO ()
main = 
    defaultMain $ 
        testGroup "tests"
            [ Test.Language.allTests 
            , Test.Runtime.allTests 
            , Test.TypeInference.allTests 
            , Test.Compiler.allTests ]



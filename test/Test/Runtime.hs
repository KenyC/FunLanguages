{-# LANGUAGE OverloadedStrings #-}
module Test.Runtime where

import Test.Tasty
import Test.Tasty.HUnit
import qualified Data.Map as Map
import Control.Monad.Identity


import Runtime

allTests :: TestTree
allTests = testGroup 
                "Runtime"
                [ programTest 
                , recursiveTest ]


programTest :: TestTree
programTest = testCase "double (double 4)" $ do
    let doubleDef =
          [ Push (Arg 1)
          , Eval
          , Push (Arg 2)
          , Eval
          , PrimOp Add 
          , Overwrite 2 -- return value + f + f x      
          , Reduce       ]
  

    let mainDef =
         [ Push (Cst 4)
         , Push (Global  "double")
         , MkAp
         , Overwrite 1
         , Reduce                ]    

    let code = Map.fromList $
                [ ("main",   mainDef   )
                , ("double", doubleDef ) ]

    let finalVal = fst $ runIdentity $ withCode code runTillEnd

    finalVal @?= Val 8


    let doubleDef =
         [ Push (Arg 1)
         , Eval
         , Push (Arg 2)
         , Eval
         , PrimOp Add     
         , Overwrite 2 -- return value + f + f x      
         , Reduce       ]

    let mainDef =
         [ Push (Cst 4)
         , Push (Global "double") 
         , MkAp
         , Push (Global "double")
         , MkAp
         , Overwrite 1
         , Reduce                ]

    let code = Map.fromList $
                [ ("main",   mainDef   )
                , ("double", doubleDef ) ]

    let finalVal = fst $ runIdentity $ withCode code runTillEnd

    finalVal @?= Val 16

    -- _

    -- double x = x + x
    let doubleDef =
         [ Push (Arg 1)
         , Eval
         , Push (Arg 2)
         , Eval
         , PrimOp Add     
         , Overwrite 2 -- return value + f + f x      
         , Reduce       ]
    
    -- doubleApply f x = f (f x)
    let doubleApplyDef =
         [ Push (Arg 2) -- x
         , Push (Arg 2) -- f
         , MkAp         -- f x
         , Push (Arg 2) -- f     
         , MkAp      -- f (f x)
         , Overwrite 3 -- return_value + f + f x      
         , Reduce       ]

    -- doubleApply double 4
    let mainDef =
            [ Push (Cst 4)
            , Push (Global "double")
            , Push (Global "doubleApply")
            , MkAp
            , MkAp
            , Overwrite 1
            , Reduce                ]


    let code = Map.fromList $
                [ ("main",        mainDef   )
                , ("double",      doubleDef ) 
                , ("doubleApply", doubleApplyDef ) ]

    let finalVal = fst $ runIdentity $ withCode code runTillEnd

    finalVal @?= Val 16

    
    -- doubleApply f x = (f x) + (f x)
    let doubleApplyDef =
         [ Push (Arg 2) -- x
         , Push (Arg 2) -- f
         , MkAp         -- f x
         , Eval
         , Push (Arg 3) -- x     
         , Push (Arg 3) -- f     
         , MkAp         -- (f x)
         , Eval
         , PrimOp Add
         , Overwrite 3 -- return_value + f + f x      
         , Reduce       ]

    -- doubleApply double 4
    let mainDef =
            [ Push (Cst 4)
            , Push (Global "double")
            , Push (Global "doubleApply")
            , MkAp
            , MkAp
            , Overwrite 1
            , Reduce                ]


    let code = Map.fromList $
                [ ("main",        mainDef   )
                , ("double",      doubleDef ) 
                , ("doubleApply", doubleApplyDef ) ]

    let finalVal = fst $ runIdentity $ withCode code runTillEnd

    finalVal @?= Val 16


recursiveTest :: TestTree
recursiveTest = testCase "fac 3" $ do
    let facDef =
          [ Push (Arg 1)        -- [n]
          , Eval           
          , Skip NZ 2           -- []
          , Push (Cst 1)        -- [1] | conditional branch 1
          , Skip NoCond 11      --     |
          , Push (Arg 1)        -- [n]               | 
          , Eval                --                   | 
          , Push (Cst 1)        -- [n, 1]            | 
          , Eval                --                   | 
          , Push (Arg 3)        -- [n, 1, n]         |  conditional branch 2
          , Eval                --                   | 
          , PrimOp Sub          -- [n, n - 1]        | 
          , Push (Global "fac") -- [n, n - 1, fac]   | 
          , MkAp                -- [n, fac (n - 1)]  | 
          , Eval                --                   |
          , PrimOp Mul          -- [n * fac (n - 1)] | 
          , Overwrite 2    
          , Reduce       ]
    

    let mainDef =
         [ Push (Cst 3)
         , Push (Global  "fac")
         , MkAp
         , Overwrite 1
         , Reduce                ]    

    let code = Map.fromList $
                [ ("main",   mainDef )
                , ("fac",    facDef  ) ]

    let finalVal = fst $ runIdentity $ withCode code runTillEnd

    finalVal @?= Val 6

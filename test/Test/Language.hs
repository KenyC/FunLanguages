{-# LANGUAGE OverloadedStrings #-}
module Test.Language where

import Test.Tasty
import Test.Tasty.HUnit
import Control.Monad.State
import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Foldable

import Language


allTests :: TestTree
allTests = testGroup 
                "Language"
                [ programTest ]


programTest :: TestTree
programTest = testCase "Simple program" $ do
  let facFun = 
        IfNZ (Var "n")
          (Mul
            (Var "n")
            (Ap 
              (Fun "fac")
              (Sub 
                (Var "n")
                (Cst 1)
              )
            )
          )
          (Cst 1)
  let mainFun = Ap (Fun "fac") (Cst 12)
  let simpleModule = Map.fromList $ 
              [ ("fac",  FunDef ["n"] facFun )
              , ("main", FunDef []    mainFun) ]
  return ()


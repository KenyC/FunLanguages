{-# LANGUAGE OverloadedStrings #-}
module Test.Compiler where

import Control.Monad.Identity
import Control.Monad.IO.Class
import Test.Tasty
import Test.Tasty.HUnit


import Runtime
import Compiler
import Language ((@@), (.+.), (.-.), (.*.), (~>))
import qualified Language as L

allTests :: TestTree
allTests = testGroup 
                "Compiler"
                [ argOffsetTest 
                , doubleFunctionTest 
                , doubleApplyTest 
                , wholeCompilationTest
                , simpleApplicationTreeTest
                , checkArgTest
                , checkUniqueUseTest          
                , checkGlobalTest          ]


argOffsetTest :: TestTree
argOffsetTest = testCase "Test arg offset" $ do
    let L.FunDef args body = L.toFunDef L.varNames $ \x (_ :: L.Var) ->  (L.Var x)
    let expectedResult = [ Push (Arg 1) ]
    compileAux args body [] @?= expectedResult

    let L.FunDef args body = L.toFunDef L.varNames $ \(_ :: L.Var) y ->  (L.Var y)
    let expectedResult = [ Push (Arg 2) ]
    compileAux args body [] @?= expectedResult

    let L.FunDef args body = L.toFunDef L.varNames $ \x y ->  (L.Var y) `L.Add` (L.Var x)
    let expectedResult = [ Push (Arg 2)  
                         , Eval         
                         , Push (Arg 2)  
                         , Eval         
                         , PrimOp Add       ]
    compileAux args body [] @?= reverse expectedResult

    let L.FunDef args body = L.toFunDef L.varNames $ \x y ->  (L.Var x) `L.Add` (L.Var y)
    let expectedResult = [ Push (Arg 1)  
                         , Eval         
                         , Push (Arg 3)  
                         , Eval         
                         , PrimOp Add       ]
    compileAux args body [] @?= reverse expectedResult


simpleApplicationTreeTest :: TestTree
simpleApplicationTreeTest = testCase "Simple application tree test" $ do
    let x = L.Fun "x"
    let y = L.Fun "y"
    let f = L.Fun "f"
    let g = L.Fun "g"
    let body = f @@ (g @@ x @@ y) @@ (f @@ x @@ y)  
    let expectedResult = [ Push (Global "y")
                         , Push (Global "x")
                         , Push (Global "f")
                         , MkAp 
                         , MkAp 
                         , Push (Global "y")
                         , Push (Global "x")
                         , Push (Global "g")
                         , MkAp 
                         , MkAp 
                         , Push (Global "f")
                         , MkAp 
                         , MkAp               ]
    compileAux [] body [] @?= reverse expectedResult


wholeCompilationTest :: TestTree
wholeCompilationTest = testCase "Whole compilation test" $ do
    let Right mainModule = compileModule $ L.buildModule $ do
            "double" ~> \x -> (L.Var x) .+. (L.Var x)
            "main"   ~> (L.Fun "double" @@ (L.Cst 4))


    liftIO $ print mainModule
    Val 8 @=? (fst $ runIdentity $ withCode mainModule runTillEnd)






doubleFunctionTest :: TestTree
doubleFunctionTest = testCase "Double function" $ do
    let funDef = L.toFunDef L.varNames $ \x -> (L.Var x) .+. (L.Var x)  
    let expectedResult =
          [ Push (Arg 1)
          , Eval
          , Push (Arg 2)
          , Eval
          , PrimOp Add 
          , Overwrite 2 -- return value + f + f x      
          , Reduce       ]
    compile funDef @?= expectedResult



doubleApplyTest :: TestTree
doubleApplyTest = testCase "Double-apply function" $ do
    let funDef = L.toFunDef L.varNames $ \f x -> (L.Var f) @@ ((L.Var f) @@ (L.Var x))  
    let expectedResult =
         [ Push (Arg 2) -- x
         , Push (Arg 2) -- f
         , MkAp         -- f x
         , Push (Arg 2) -- f     
         , MkAp      -- f (f x)
         , Overwrite 3 -- return_value + f + f x      
         , Reduce       ]

    compile funDef @?= expectedResult



checkArgTest :: TestTree
checkArgTest = testCase "checkArg function" $ do
    let mainModule = L.buildModule $ do
            "bla"  ~> \x (y :: L.Var) -> (L.Var x) .+. (L.Var "abc")
            "blop" ~> \x y -> (L.Var x) .+. (L.Var y)

    checkArg mainModule @?= Just (UnknownArg "abc")

    let mainModule = L.buildModule $ do
            "bla"  ~> \x y -> (L.Var y) .+. (L.Var x)
            "blop" ~> \x y -> (L.Var x) .+. (L.Var y)

    checkArg mainModule @?= Nothing




checkGlobalTest :: TestTree
checkGlobalTest = testCase "checkGlobal function" $ do
    let mainModule = L.buildModule $ do
            "blop" ~> \x y -> (L.Var x) .+. (L.Var y)
            "bla"  ~> \x   -> ((L.Fun "blip") @@ (L.Var x))

    checkGlobal mainModule @?= Just (UnknownGlobal "blip")

    let mainModule = L.buildModule $ do
            "bla"  ~> \x y -> (L.Var y) .+. ((L.Fun "blop") @@ (L.Var x))
            "blop" ~> \x y -> (L.Var x) .+. (L.Var y)

    checkGlobal mainModule @?= Nothing




checkUniqueUseTest :: TestTree
checkUniqueUseTest = testCase "checkUniqueUse function" $ do
    let mainModule = L.buildModule $ do
            "add"      ~> \x y -> (L.Var x) .+. (L.Var y)
            "apply"    ~> \f x -> (L.Var f) @@ (L.Var x)

    checkUniqueUse mainModule @?= Nothing

    let mainModule = L.buildModule $ do
            "double"  ~> \x -> (L.Var x) .+. (L.Var x)
            "add"     ~> \x y -> (L.Var x) .+. (L.Var y)

    checkUniqueUse mainModule @?= Just (ReuseVar "x")



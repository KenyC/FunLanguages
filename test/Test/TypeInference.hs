{-# LANGUAGE OverloadedStrings #-}
module Test.TypeInference where

import Test.Tasty
import Test.Tasty.HUnit
import Control.Monad.State
import qualified Data.Map as Map
import qualified Data.Set as Set

import Language
import TypeInference

allTests :: TestTree
allTests = testGroup 
                "Type inference"
                [ tyInferTest ]




tyInferTest :: TestTree
tyInferTest = testCase "type inference functions" $ do
    let ty1 = (FreeVar "a") :-> Val
        ty2 = Val :-> (FreeVar "b")

    case runTypeMonad Map.empty (unify $ ty1 :~: ty2) of
        Just (_, Val :-> Val) -> return ()
        _ ->  assertFailure "Didn't unify to Var :-> Var !"

    let subst = Map.fromList $
           [ ("a", Val :-> (FreeVar "b"))
           , ("b", (FreeVar "a") :-> Val) ]

    case runTypeMonad subst applySubst of
      Just (subst', _) -> do
          subst' @?= Map.fromList [  ("a", Val :-> (FreeVar "a" :-> Val))
                                  ,  ("b", (Val :-> FreeVar "b") :-> Val)] 

      Nothing -> assertFailure "'subst' failed"

    let infiniteSubst = Map.fromList $ 
              [  ("a", Val :-> (FreeVar "a" :-> Val))
              ,  ("b", (Val :-> FreeVar "b") :-> Val)] 

    case runTypeMonad infiniteSubst infiniteTypeCheck of 
              Just _ -> assertFailure "Should detect infinite type"
              _      -> return ()

    let expr = Add (Ap (Var "f") (Var "x")) (Cst 3)
    let annotatedExpr = evalState (labelAllSubExpr "foo" expr) 0

    let extractAnonymous (Anonymous x) = Just x 
        extractAnonymous _ = Nothing

    Just 2 @=? maximum (fmap extractAnonymous annotatedExpr)
    True   @=? any (== (NamedVar "foo" "x")) annotatedExpr
    True   @=? any (== (NamedVar "foo" "f")) annotatedExpr

    let equations = Set.fromList $ gatherEquations annotatedExpr
    let expectedResult = Set.fromList $ 
                 [ FreeVar (Anonymous 2) :~: Val
                 , FreeVar (Anonymous 0) :~: Val 
                 , FreeVar (Anonymous 1) :~: Val
                 , (FreeVar $ NamedVar "foo" "f") :~: (FreeVar (NamedVar "foo" "x") :-> (FreeVar $ Anonymous 0)) ]
    equations @?= expectedResult